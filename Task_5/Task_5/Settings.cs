﻿namespace Task_5
{
    public class Settings
    {
        public string Filepath { get; set; }

        private static Settings _instance;

        private Settings() { }

        public static Settings Instance => _instance ?? (_instance = new Settings());
    }
}