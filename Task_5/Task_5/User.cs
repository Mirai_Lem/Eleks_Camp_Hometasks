﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;

namespace Task_5
{
    public class User
    {
        private string Name { get;}
        private string SName { get;}
        private int Age { get;}
        private string Workplace { get;}

        public bool NotEmpty => !Name.IsEmpty() && !SName.IsEmpty() && Age > 0;

        public User(string name, string sname, string wp, int age)
        {
            Name = name;
            SName = sname;
            Age = age;
            Workplace = wp;
        }

        public override string ToString()
        {
            return Name + "_" + SName + "_" + Age + "_" + (Workplace ?? "") + "$";
        }
    }
}
