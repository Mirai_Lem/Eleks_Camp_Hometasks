﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5
{
    public class CsvSerializer
    {

        private List<string> Parse(string line)
        {
            const int minTokenNum = 3;
            char[] delimiters = {'_'};
            var words = line.Split(delimiters);

            return words.Length < minTokenNum ? null : words.ToList();
        }

        private bool FullToken(string line)
        {
            var three = false;

            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == '$' && line[i - 1] == '_')
                {
                    three = true;
                }
            }
            return !three;
        }

        private string Serialize(string input)
        {
            var tokens = Parse(input);
            var full = FullToken(input);
            const char c = ',';

            return "\"" + tokens[0] + "\"" + c + "\"" + tokens[1] + "\"" + c + "\"" + tokens[2] + "\"" + (full ? (c + "\"" + tokens[3] + "\"") : null);
        }

        public void WriteFile(User user)
        {
            var line = Serialize(user.ToString());
            var reader = new StreamReader(Settings.Instance.Filepath);
            var result = reader.ReadToEnd() + line;
            reader.Close();
            var writer = new StreamWriter(Settings.Instance.Filepath);
            writer.WriteLine(result);
            writer.Close();
        }
    }
}
