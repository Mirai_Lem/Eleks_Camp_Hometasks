﻿namespace Task_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.WorkTxt = new System.Windows.Forms.TextBox();
            this.AgeTxt = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.SNameTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.AddBtn = new System.Windows.Forms.Label();
            this.FileDialog = new System.Windows.Forms.OpenFileDialog();
            this.FilePathTxt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.AgeTxt)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(533, 494);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Last action";
            // 
            // NameTxt
            // 
            this.NameTxt.Location = new System.Drawing.Point(68, 157);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(108, 22);
            this.NameTxt.TabIndex = 4;
            // 
            // WorkTxt
            // 
            this.WorkTxt.Location = new System.Drawing.Point(508, 155);
            this.WorkTxt.Name = "WorkTxt";
            this.WorkTxt.Size = new System.Drawing.Size(127, 22);
            this.WorkTxt.TabIndex = 6;
            // 
            // AgeTxt
            // 
            this.AgeTxt.Location = new System.Drawing.Point(372, 156);
            this.AgeTxt.Name = "AgeTxt";
            this.AgeTxt.Size = new System.Drawing.Size(120, 22);
            this.AgeTxt.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Name";
            // 
            // SNameTxt
            // 
            this.SNameTxt.Location = new System.Drawing.Point(206, 156);
            this.SNameTxt.Name = "SNameTxt";
            this.SNameTxt.Size = new System.Drawing.Size(128, 22);
            this.SNameTxt.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(216, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Second Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(400, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Age";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(533, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Workplace";
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(68, 381);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 41);
            this.label6.TabIndex = 13;
            this.label6.Text = "Choose file";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Click += new System.EventHandler(this.FilePathTxt_Click);
            // 
            // AddBtn
            // 
            this.AddBtn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AddBtn.Location = new System.Drawing.Point(521, 226);
            this.AddBtn.Name = "AddBtn";
            this.AddBtn.Size = new System.Drawing.Size(99, 49);
            this.AddBtn.TabIndex = 15;
            this.AddBtn.Text = "Add Button";
            this.AddBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AddBtn.Click += new System.EventHandler(this.AddBtn_Click);
            // 
            // FileDialog
            // 
            this.FileDialog.FileName = "openFileDialog1";
            this.FileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.FileDialog_FileOk);
            // 
            // FilePathTxt
            // 
            this.FilePathTxt.AutoSize = true;
            this.FilePathTxt.Location = new System.Drawing.Point(84, 432);
            this.FilePathTxt.Name = "FilePathTxt";
            this.FilePathTxt.Size = new System.Drawing.Size(70, 17);
            this.FilePathTxt.TabIndex = 16;
            this.FilePathTxt.Text = "Path here";
            this.FilePathTxt.Click += new System.EventHandler(this.FilePathTxt_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 544);
            this.Controls.Add(this.FilePathTxt);
            this.Controls.Add(this.AddBtn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SNameTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AgeTxt);
            this.Controls.Add(this.WorkTxt);
            this.Controls.Add(this.NameTxt);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.AgeTxt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.TextBox WorkTxt;
        private System.Windows.Forms.NumericUpDown AgeTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SNameTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label AddBtn;
        private System.Windows.Forms.OpenFileDialog FileDialog;
        private System.Windows.Forms.Label FilePathTxt;
    }
}

