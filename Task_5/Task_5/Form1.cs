﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_5
{
    public partial class Form1 : Form
    {

        public static FilePath _FilePath;

        public static AddUser _AddUser;

        public Form1()
        {
            InitializeComponent();
            Presenter _presenter = new Presenter();
        }

        private void FileDialog_FileOk(object sender, CancelEventArgs e)
        {
            FilePathTxt.Text = FileDialog.FileName;
            _FilePath(FileDialog.FileName);
        }

        private void FilePathTxt_Click(object sender, EventArgs e)
        {
            FileDialog.ShowDialog();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            List<string> Array = new List<string>();
            Array.Add(NameTxt.Text);
            Array.Add(SNameTxt.Text);
            Array.Add(WorkTxt.Text);
            Array.Add(AgeTxt.Text);
            _AddUser(Array);
        }
    }
}
