﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5
{

    public delegate void FilePath(string path);

    public delegate void AddUser(List<string> data);

    class Presenter
    {
        public Presenter()
        {
            Form1._FilePath += _changePath;
            Form1._AddUser += _addUser;
        }

        private void _changePath(string path)
        {
            Settings.Instance.Filepath = path;
        }

        private void _addUser(List<string> data)
        {
            int n;
            if(!Int32.TryParse(data[3], out n))
                return;

            User user = new User(data[0], data[1], data[2], n);
            CsvSerializer Serializer = new CsvSerializer();

            Serializer.WriteFile(user);
        }
    }
}
