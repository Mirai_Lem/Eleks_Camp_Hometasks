#pragma once
#include "Contact.h"
#include <vector>

class Organizer {

private:
	std::vector<Contact> m_List;

public:

	Organizer();
	~Organizer();

	std::string GetAllContacts();

	std::string GetContact(std::string name);

	bool AddContact(std::vector<std::string> data);

	bool DelContact(std::string name);
	
};