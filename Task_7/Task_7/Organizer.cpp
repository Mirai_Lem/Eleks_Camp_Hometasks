#include "Organizer.h"
#include <iostream>

Organizer::Organizer() {}
Organizer::~Organizer() {}

std::string Organizer::GetAllContacts() {
	std::string data = "";
	for (int i = 0; i < m_List.capacity() && m_List.size(); i++) {
		data += "Name: " + m_List[i].m_Name + " number: " + m_List[i].m_Number + " mail: " + m_List[i].m_Email + "\n";
	}
	if (m_List.size() == 0) {
		data = "Error 402: List is empty\n";
	}
	return data;
}

bool Organizer::AddContact(std::vector<std::string> data) {
	if (data.capacity() < 3 || data.capacity() > 4 )
		return 0;
	if (data.capacity() == 3)
		data.push_back("");
	Contact user{ data[1], data[2], data[3] };
	m_List.push_back(user);
	return 1;
}

std::string Organizer::GetContact(std::string name) {
	auto beg = m_List.begin();
	auto end = m_List.end();
	while (beg != end) {
		if (beg->m_Name == name)
			return beg->ToSrting();
		beg++;
	}
	return "Error 404: No matches found\n";
}

bool Organizer::DelContact(std::string name) {
	auto beg = m_List.begin();
	auto end = m_List.end();
	while (beg != end) {
		if (beg->m_Name == name) {
			m_List.erase(beg);
			return 1;
		}
		beg++;
	}
	return 0;
}
