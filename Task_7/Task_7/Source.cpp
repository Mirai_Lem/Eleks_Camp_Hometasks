#include <iostream>
#include "Contact.h"
#include "Organizer.h"
#include <sstream>
#include <string.h>
#include <stdio.h>

void Add(std::vector<std::string> data, Organizer &obj);
void Del(std::vector<std::string> data, Organizer &obj);
void Find(std::vector<std::string> data, Organizer &obj);
void GetAll(Organizer &obj);

int main() {
	Organizer Manager;
		std::cout << "\tWelcome to Contact manager!\nWhat do you want to do?\n" << std::endl;
		std::cout << "'add <name> <number> <email>' - to add a new contact\n'get <name>' - to gets contact by name\n" <<
			"'getall' - returns total list\n'del <name>' - deletes contact by name\n" << std::endl;

		while (1) {

			std::string line;
			std::getline(std::cin, line);
			if (line == "")
				continue;
			std::stringstream command;
			command << line;
			std::vector<std::string> data;
			std::string token;

			while (std::getline(command, token, ' ')) {
				data.push_back(token);
			}


			if (data[0] == "add") {
				Add(data, Manager);
			}
			else if (data[0] == "get") {
				Find(data, Manager);
			}
			else if (data[0] == "getall") {
				GetAll(Manager);
			}
			else if (data[0] == "del") {
				Del(data, Manager);
			}
			else std::cout << ">Wrong command format\n";
		}
}

void Add(std::vector<std::string> data, Organizer &obj){
	if (obj.AddContact(data))
		std::cout << ">Done\n";
	else std::cout << ">Input error\n";
}
void Del(std::vector<std::string> data, Organizer &obj) {
	if(obj.DelContact(data[1]))
		std::cout << ">Done\n";
	else std::cout << ">Input error\n";
}
void GetAll(Organizer &obj) {
	std::cout << ">" << obj.GetAllContacts();
}
void Find(std::vector<std::string> data, Organizer &obj) {
	std::cout << ">" << obj.GetContact(data[1]);
}
