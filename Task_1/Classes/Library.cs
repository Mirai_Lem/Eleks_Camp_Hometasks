﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task1.Interfaces;

namespace Task1.Classes
{
    public class Library : IComparable<Library>, ICountingBooks
    {
        public string mName;
        private string mOwner;
        private int mDepNum;
        public Department[] mDepartments;

        public Library(string name, string owner, Department[] arr)
        {
            mName = (name.Length > 0 ? name : "unknown");
            mOwner = (owner.Length > 0 ? owner : "unknown");
            if (arr.Length > 0)
            {
                mDepartments = arr;
                mDepNum = arr.Length;
            }
        }

        public void AddDepartment(Department d)
        {
            List<Department> arr = mDepartments.ToList();
            arr.Add(d);
            mDepartments = arr.ToArray();
        }

        public void AddDepartment(string name)
        {
            Department d = new Department(name, null);
            AddDepartment(d);
        }

        public override string ToString()
        {
            return "Name: " + mName + ", owner: " + mOwner + ", total book number: " + GetTotalBooks().ToString();
        }

        public void SetOwner(string name)
        {
            mOwner = name;
        }

        public void SetName(string name)
        {
            mName = name;
        }

        public void GetDepList()
        {
            foreach (Department dep in mDepartments)
            {
                Console.WriteLine(dep.ToString());
            }
        }

        int IComparable<Library>.CompareTo(Library other)
        {
            return this.mDepartments.Length - other.mDepartments.Length;
        }

        public int GetTotalBooks()
        {
            int total = 0;
            foreach (Department d in mDepartments)
            {
                total += d.GetBookNum();
            }
            return total;
        }
    }
}
