﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Classes
{
    public class Author
    {
        public string mName;
        public Book[] mBooks;

        public Author(string name, Book[] books)
        {
            mName = (name.Length > 0 ? name : "anonim");
            mBooks = (books.Length > 0 ? books : null);
        }

        public override string ToString()
        {
            return mName;
        }

        public void SetPseudo(string pseudo)
        {
            mName = pseudo;
        }

        public void NewBook(string name, int pages)
        {
            Book b = new Book(name, this, pages);
        }
    }
}
