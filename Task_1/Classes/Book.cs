﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Classes
{
    public class Book : IComparable<Book>
    {
        public string mName;
        public Author mAuthor;
        private int mPageNum;

        public Book(string name, Author author, int pagenum)
        {
            mName = (name.Length > 0 ? name : "unknown");
            mAuthor = author;
            mPageNum = (pagenum > 0 ? pagenum : 0);
        }

        public override string ToString()
        {
            return " Book: " + mName + " author: " + mAuthor.ToString() + " with " + mPageNum + " pages";
        }

        int IComparable<Book>.CompareTo(Book other)
        {
            return mPageNum - other.mPageNum;
        }
    }
}
