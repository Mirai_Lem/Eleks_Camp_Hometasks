﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task1.Interfaces;

namespace Task1.Classes
{
    public class Department : ICountingBooks, IComparable<Department>
    {
        public string mName;
        private int mBookNum;
        public Book[] Books;

        public Department(string name, Book[] arr)
        {
            mName = (name.Length > 0 ? name : "unknown");
            if (arr.Length > 0)
            {
                Books = arr;
                mBookNum = arr.Length;
            }
        }

        public override string ToString()
        {
            return "Department: " + mName + " with " + mBookNum + " books";
        }

        public void AddBook(Book book)
        {
            Book[] Arr = new Book[Books.Length + 1];
            int i;
            for (i = 0; i < Books.Length; ++i)
            {
                Arr[i] = Books[i];
            }
            Arr[++i] = book;
            Books = Arr;
        }

        public void ChangeName(string name)
        {
            mName = name;
        }

        public void RenewBookNum()
        {
            mBookNum = Books.Length;
        }
        public int GetBookNum()
        {
            return mBookNum;
        }

        public void GetBookList()
        {
            foreach (Book b in Books)
            {
                Console.WriteLine(b.ToString());
            }
        }

        int IComparable<Department>.CompareTo(Department other)
        {
            return this.GetBookNum() - other.GetBookNum();
        }

        int ICountingBooks.GetTotalBooks()
        {
            mBookNum = Books.Length;
            return mBookNum;
        }
    }
}
