﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Task1.Interfaces;

namespace Task1.Classes
{
    [DataContract]
    public class Department : ICountingBooks, IComparable<Department>
    {
        [DataMember]
        public string mName;
        [DataMember]
        public List<Book> Books;

        public Department(string name)
        {
            mName = (name.Length > 0 ? name : "unknown");
            Books = new List<Book>();
        }

        public override string ToString()
        {
            return "Department: " + mName + " with " + Books.Count + " books";
        }

        public void AddBook(Book book)
        {
            Books.Add(book);
        }

        public void ChangeName(string name)
        {
            mName = name;
        }
        
        public int GetBookNum()
        {
            return Books.Count;
        }

        public void GetBookList()
        {
            Console.WriteLine("\tDepartment: {0}", mName);
            foreach (Book b in Books)
            {
                Console.WriteLine(b.ToString());
            }
        }

        int IComparable<Department>.CompareTo(Department other)
        {
            return this.GetBookNum() - other.GetBookNum();
        }

        int ICountingBooks.GetTotalBooks()
        {
            return Books.Count;
        }
    }
}
