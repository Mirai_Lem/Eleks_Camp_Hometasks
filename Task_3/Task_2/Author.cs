﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Classes
{
    [DataContract]
    public class Author
    {
        [DataMember]
        public string mName;
       // [DataMember]
       // public List<Book> mBooks;

        public Author()
        {

        }
        
        public Author(string name)
        {
            mName = (name.Length > 0 ? name : "anonim");
            //mBooks = new List<Book>();
        }

        public override string ToString()
        {
            return mName;
        }

        public void SetPseudo(string pseudo)
        {
            mName = pseudo;
        }

        //public void NewBook(string name, int pages)
        //{
        //    Book b = new Book(name, this, pages);
        //}

        //public void AddBook(Book b)
        //{
        //    //mBooks.Add(b);
        //}

        public List<Author> ToList()
        {
            List<Author> ret = new List<Author>();
            ret.Add(this);
            return ret;
        }
    }
}
