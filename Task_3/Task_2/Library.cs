﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Task1.Interfaces;

namespace Task1.Classes
{
    [DataContract]
    public class Library : IComparable<Library>, ICountingBooks
    {
        [DataMember]
        public string mName;
        [DataMember]
        private string mOwner;
        [DataMember]
        public List<Department> mDepartments;

        public Library()
        {
            mName = "default";
            mOwner = mName;
            mDepartments = null;
        }

        public Library(string name, string owner)
        {
            mName = (name.Length > 0 ? name : "unknown");
            mOwner = (owner.Length > 0 ? owner : "unknown");
            mDepartments = new List<Department>();
        }

        public void AddDepartment(Department d)
        {
            mDepartments.Add(d);
        }

        public void AddDepartment(string name)
        {
            Department d = new Department(name);
            AddDepartment(d);
        }

        public override string ToString()
        {
            return "Name: " + mName + ", owner: " + mOwner + ", total book number: " + GetTotalBooks().ToString();
        }

        public void SetOwner(string name)
        {
            mOwner = name;
        }

        public void SetName(string name)
        {
            mName = name;
        }

        public void GetDepList()
        {
            foreach (Department dep in mDepartments)
            {
                Console.WriteLine(dep.ToString());
            }
        }

        int IComparable<Library>.CompareTo(Library other)
        {
            return this.mDepartments.Count - other.mDepartments.Count;
        }

        public int GetTotalBooks()
        {
            int total = 0;
            foreach (Department d in mDepartments)
            {
                total += d.GetBookNum();
            }
            return total;
        }

        public void GetBooksInfo()
        {
            foreach(Department d in mDepartments)
            {
                d.GetBookList();
            }
        }
    }
}
