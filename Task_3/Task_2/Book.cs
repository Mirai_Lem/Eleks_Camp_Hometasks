﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Classes
{
    [DataContract]
    public class Book : IComparable<Book>
    {
        [DataMember]
        public string mName;
        [DataMember]
        public List<Author> mAuthor;
        [DataMember]
        private int mPageNum;

        public Book(string name, int pagenum, params Author [] author)
        {
            mName = (name.Length > 0 ? name : "unknown");
            mAuthor = author.ToList();
            mPageNum = (pagenum > 0 ? pagenum : 0);
        }

        public override string ToString()
        {
            string line = "";
            foreach (Author a in mAuthor)
            {
                line += a.ToString() + " ";
            }
            return " Book: " + mName + " | authors: " + line + "| " + mPageNum + " pages";
        }

        int IComparable<Book>.CompareTo(Book other)
        {
            return mPageNum - other.mPageNum;
        }
    }
}
