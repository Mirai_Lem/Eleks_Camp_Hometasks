﻿using System;
using System.CodeDom;
using System.IO;
using Task1.Classes;
using System.Runtime.Serialization.Json;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Library lib = new Library("Lab", "Michael");
            Author aut1 = new Author("Mika");
            Author aut2 = new Author("Peteri");
            Department dep = new Department("Alchemy");
            Department dep0 = new Department("Physycs");

            Book b1 = new Book("Ambroziah", 825, aut1);
            Book b2 = new Book("Tanatonaut Tom_1", 550, aut2);
            Book b3 = new Book("Tanatonaut Tam_2", 630, aut2);
            Book b4 = new Book("E=mc^2", 420, aut2);
            Book b5 = new Book("Iron Gold", 980, aut1);
            Book b6 = new Book("Supermassive", 244, aut2);
            Book b7 = new Book("Virgin blood", 635, aut1);
            Book b8 = new Book("Of Moonstone", 66, aut1, aut2);

            dep.AddBook(b1);
            dep.AddBook(b5);
            dep.AddBook(b7);
            dep.AddBook(b8);

            dep0.AddBook(b2);
            dep0.AddBook(b3);
            dep0.AddBook(b4);
            dep0.AddBook(b6);

            lib.AddDepartment(dep);
            lib.AddDepartment(dep0);

            Console.WriteLine(lib.GetTotalBooks());

            lib.GetBooksInfo();

            using (MemoryStream stream1 = new MemoryStream())
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Library));
                ser.WriteObject(stream1, lib);

                stream1.Position = 0;
                StreamReader sr = new StreamReader(stream1);

                Console.Write("Serialized data:\n");
                Console.WriteLine(sr.ReadToEnd());
            }

            Console.ReadKey();
        }
    }
}
