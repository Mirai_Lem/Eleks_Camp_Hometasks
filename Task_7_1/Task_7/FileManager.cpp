#include "FileManager.h"
#include <fstream>
#include <string>

void FileManager::SetPath(std::string path)
{
	m_FilePath = path;
}

std::string FileManager::GetPath()
{
	return m_FilePath;
}

std::string FileManager::ReadFile()
{
	std::ifstream reader (m_FilePath);
	if (reader.is_open()) {
		std::string List = "";
		std::string line;
		while (std::getline(reader, line)) {
			List += line + "\n";
		}
		reader.close();
		return List;
	}
	else {
		reader.close();
		return "Error 405: Wrong file path\n";
	}
}

bool FileManager::WriteFile(std::vector<Contact> list) {
	std::ofstream writer;
	writer.open(m_FilePath, std::ofstream::out | std::ofstream::trunc);

	if (!writer.is_open())
		return 0;

	for (int i = 0; i < list.size(); i++) {
		writer << list[i].ToSrting();
	}
	writer.close();
	return 1;
}
