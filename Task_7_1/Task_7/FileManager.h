#pragma once
#include <iostream>
#include <vector>
#include "Contact.h"

class FileManager {
    std::string m_FilePath;

public:
	void SetPath(std::string path);
	std::string GetPath();
	std::string ReadFile();
	bool WriteFile(std::vector<Contact> list);
};