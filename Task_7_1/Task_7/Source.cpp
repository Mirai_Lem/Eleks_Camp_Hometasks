#include <iostream>
#include "Contact.h"
#include "Organizer.h"
#include <sstream>
#include <string.h>
#include <stdio.h>

void Add(std::vector<std::string> data, Organizer &obj);
void Del(std::vector<std::string> data, Organizer &obj);
void Find(std::vector<std::string> data, Organizer &obj);
void GetAll(Organizer &obj);
void Open(std::vector<std::string> data, Organizer &obj);
void Write(std::vector<std::string> data, Organizer &obj);

int main() {
	Organizer Manager;
		std::cout << "\tWelcome to Contact manager!\nWhat do you want to do?\n" << std::endl;
		std::cout << "'add <name> <number> <email>' - to add a new contact" << std::endl;
		std::cout << "'get <name>' - to gets contact by name" << std::endl;
		std::cout << "'getall' - returns total list" << std::endl;
		std::cout << "'del <name>' - deletes contact by name" << std::endl;
		std::cout << "'open <file_path>' - reads data from file" << std::endl;
		std::cout << "'save <file_path>' - writes data to file" << std::endl << std::endl;

		while (1) {

			std::string line;
			std::getline(std::cin, line);
			if (line == "")
				continue;
			std::stringstream command;
			command << line;
			std::vector<std::string> data;
			std::string token;

			while (std::getline(command, token, ' ')) {
				data.push_back(token);
			}


			if (data[0] == "add") {
				Add(data, Manager);
				std::cout << std::endl;
			}
			else if (data[0] == "get") {
				Find(data, Manager);
				std::cout << std::endl;
			}
			else if (data[0] == "getall") {
				GetAll(Manager);
				std::cout << std::endl;
			}
			else if (data[0] == "del") {
				Del(data, Manager);
				std::cout << std::endl;
			}
			else if (data[0] == "open") {
				Open(data, Manager);
				std::cout << std::endl;
			}
			else if (data[0] == "save") {
				Write(data, Manager);
				std::cout << std::endl;
			}
			else std::cout << ">Wrong command format\n" << std::endl;
		}
}

void Add(std::vector<std::string> data, Organizer &obj){
	if (obj.AddContact(data))
		std::cout << ">Done" << std::endl;
	else std::cout << ">Input error" << std::endl;
}
void Del(std::vector<std::string> data, Organizer &obj) {
	if(obj.DelContact(data[1]))
		std::cout << ">Done" << std::endl;
	else std::cout << ">Input error" << std::endl;
}
void GetAll(Organizer &obj) {
	std::cout << ">" << obj.GetAllContacts();
}
void Find(std::vector<std::string> data, Organizer &obj) {
	std::cout << ">" << obj.GetContact(data[1]);
}

void Open(std::vector<std::string> data, Organizer &obj) {
	std::cout << ">" << obj.OpenFile(data[1]);
}

void Write(std::vector<std::string> data, Organizer &obj) {
	if (obj.WriteFile(data[1]))
		std::cout << ">Done" << std::endl;
	else std::cout << ">Inpur error" << std::endl;
}
