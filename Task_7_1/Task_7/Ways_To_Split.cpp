/*
Here i`ll keep possible ways to split the string into words
These ways bother me in my source code, but i don`t want to lose them
*/

#include <iostream>

void Some_Bad_Way_I_Was_Tought_In_University() {

	// Manager - is some object of "Organizer" that keeps data

	/*const char* delim = " ";

	char* command;
	std::cin >> command;*/

	//char* ptr;
	//ptr = std::strtok(command, delim);
	/*
	for(int i = 0; ptr != NULL; i++) {
	data[i] = ptr;
	ptr = strtok(NULL, delim);
	}

	if (data[0] == "add") {
	Add(data, Manager);
	}
	if (data[0] == "get") {
	Find(data, Manager);
	}
	if (data[0] == "getall") {
	GetAll(Manager);
	}
	if (data[0] == "del") {
	Del(data, Manager);
	}
	*/
}

void Rather_Good_Way_Written_By_Cool_Guy() {

	// Can be used in recursion to get all words

	auto split = [](const std::string &s, const std::string &sep, std::string &lhs, std::string &rhs) -> bool {
		size_t i = s.find(sep);
		if (i == std::string::npos)
			return false;
		lhs = s.substr(0, i);
		rhs = s.substr(i + s.length());
		return true;
	};
}